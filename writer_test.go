package progress

import (
	"bytes"
	"io"
	"testing"
)

var _ io.Writer = (*Writer)(nil)

func TestNewWriter(t *testing.T) {
	pw := NewWriter(io.Discard)

	if pw.w == nil {
		t.Error("expected w to not be nil")
	} //if
} //func

func TestWriterWrite(t *testing.T) {
	t.Run("WithoutCallback", func(t *testing.T) {
		var out bytes.Buffer
		pw := NewWriter(&out)

		c, err := pw.Write([]byte("123"))
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := 3, c; actual != expected {
			t.Errorf("expected wrote %d but was %d", expected, actual)
		} //if

		if expected, actual := 0, pw.total; actual != expected {
			t.Errorf("expected total %d but was %d", expected, actual)
		} //if

		_, err = pw.Write([]byte("123"))
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := 0, pw.total; actual != expected {
			t.Errorf("expected total %d but was %d", expected, actual)
		} //if
	}) //func
	t.Run("WithCallback", func(t *testing.T) {
		var out bytes.Buffer
		pw := NewWriter(&out)

		wrote := false
		pw.Callback = func(i int) {
			if expected, actual := out.Len(), i; actual != expected {
				t.Errorf("expected progress %d but was %d", expected, actual)
			} //if
			wrote = true
		} //func

		c, err := pw.Write([]byte("123"))
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := 3, c; actual != expected {
			t.Errorf("expected wrote %d but was %d", expected, actual)
		} //if

		if !wrote {
			t.Error("expected callback to be called")
		} //if

		_, err = pw.Write([]byte("123"))
		if err != nil {
			t.Fatal("unexpected error:", err)
		} //if

		if expected, actual := 6, pw.total; actual != expected {
			t.Errorf("expected total %d but was %d", expected, actual)
		} //if
	}) //func
} //func
