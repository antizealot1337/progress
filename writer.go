package progress

import "io"

// Writer that can inform of progress via a Callback.
type Writer struct {
	w        io.Writer
	total    int
	Callback func(int)
} //struct

// NewWriter creates a new Writer.
func NewWriter(w io.Writer) *Writer {
	return &Writer{
		w:     w,
		total: 0,
	}
} //func

func (p *Writer) Write(b []byte) (int, error) {
	c, err := p.w.Write(b)
	if p.Callback != nil {
		p.total += c
		p.Callback(p.total)
	} //if
	return c, err
} //func
